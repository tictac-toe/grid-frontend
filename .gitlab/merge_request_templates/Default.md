**To be done before merging:**

* [ ] Develop solution
* [ ] Manually test your solution
* [ ] Update documentation
* [ ] Add unit-tests related to your update
* [ ] Add functional tests related to your update
* If you add new files in root folder
  * Add `COPY` section in `Dockerfile`
* If you add env variables
  * [ ] Add them into `.env.example`
  * [ ] Add them into  `conf/values/<ENV>.yaml` for ALL environments
  * [ ] Add secrets variables encrypted in `conf/values/secrets.<ENV>.yaml`
* [ ] Resolve `WIP` status and assign a reviewer
