#!/usr/bin/env sh

# This script permit to set the api url before running nginx

# Replace API url by current env URL
sed -i "s/###API_URL###/$API_PROTOCOL:\/\/$API_URL\//g" /usr/share/nginx/html/dist/build.js.map
sed -i "s/###API_URL###/$API_PROTOCOL:\/\/$API_URL\//g" /usr/share/nginx/html/dist/build.js

# Run CMD passed to container
exec "$@"
